#include "ventanaprincipal.h"
#include "ui_ventanaprincipal.h"

VentanaPrincipal::VentanaPrincipal(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::VentanaPrincipal)
{
    ui->setupUi(this);
    QObject::connect(this->ui->boton_dec_bin,SIGNAL(clicked()),this,SLOT(dec_bin()));
    QObject::connect(this->ui->bot_bin_dec,SIGNAL(clicked()),this,SLOT(bin_dec()));
    QObject::connect(this->ui->bot_bin_hexa,SIGNAL(clicked()),this,SLOT(bin_hex()));
    QObject::connect(this->ui->bot_bin_oct,SIGNAL(clicked()),this,SLOT(bin_oct()));
}

VentanaPrincipal::~VentanaPrincipal()
{
    delete ui;
}
void VentanaPrincipal::bin_dec()
{
    bool ok = false;
    QString cadena = this->ui->binario_linea->text();
    QString numero = QString::number(cadena.toLongLong(&ok,2));
    if(ok)
        this->ui->respuesta_linea->setText(numero);
    else
        this->ui->respuesta_linea->setText(QString("Error"));
}
void VentanaPrincipal::bin_hex()
{
     bool ok = false;
     QString cadena = this->ui->binario_linea->text();
     QString numero = QString::number(cadena.toLongLong(&ok,2),16);
     if(ok)
         this->ui->respuesta_linea->setText(numero);
     else
         this->ui->respuesta_linea->setText(QString("Error"));
}
void VentanaPrincipal::dec_bin()
{
     bool ok = false;
     QString cadena = this->ui->decimal_linea->text();
     QString numero = QString::number(cadena.toLongLong(&ok),2);
     if(ok)
         this->ui->respuesta_linea->setText(numero);
     else
         this->ui->respuesta_linea->setText(QString("Error"));
}
void VentanaPrincipal::bin_oct()
{
     bool ok = false;
     QString cadena = this->ui->binario_linea->text();
     QString numero = QString::number(cadena.toLongLong(&ok,2),8);
     if(ok)
         this->ui->respuesta_linea->setText(numero);
     else
         this->ui->respuesta_linea->setText(QString("Error"));
}
